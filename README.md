# Dynamic Resume

## A Gitlab CI/CD pipeline to render a resume built using markdown to PDF and publishing it to Gitlab pages using Pandoc.

Credits: [John Bokma](tab:https://github.com/john-bokma/resume-pandoc)

Full instructions and explanation is provided on my [blog post](https://vbh.ai/gitlab-cv/). The TLDR; is listed below.
