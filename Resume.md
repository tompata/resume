---
name: Tamás Tompa
keywords: engineering, technology, leadership, product development
left-column:
 - 'Technology Leader'
 - 'E-mail: [tompata@gmail.com](mailto:tompata@gmail.com)'
 - 'Phone: +36 30 3700748'
right-column:
 - 'Location: Budapest, Hungary'
 - 'Linkedin: [tompatamas](https://www.linkedin.com/in/tompatamas/)'
 - 'Last Updated: \today'
...

# Summary

A mindful and pragmatic engineer with strong leadership skills. 
20+ years experience in agile product development and team leadership, with a data-driven approach. 
Excellent communication and interpersonal skills with an ever-learning and adaptive attitude.
A mountaineer, nature lover, volunteer and father.

# Technical Skills

Java • Kotlin • Ruby • Python • TypeScript • Shell • SQL • NoSQL • TDD • BDD • Data Analytics • Kubernetes • DevOps

# Education

## Óbuda University, Budapest

**2006 - 2011**

- John von Neumann Faculty of Informatics, Engineering Information Technologist (BSC)
- Thesis for the BSC degree: Secure (Security aspects of) Video Streaming
- Core Courses: Theory of Computing, Mathematics, Programming, Computer Architectures, Computer Networks, Software Engineering

## Budapest University of Technology and Economics

**1999 - 2005**

- Electrical and Electronics Engineering

# Experience

## Chemaxon - Director of Software Engineering

**March 2023 - Present** 

- Lead a dynamic engineering team of over 60 professionals, driving innovation and ensuring the delivery of high-quality software products
- Collaborate closely with cross-functional teams, including Product, Tech, Design and Quality Assurance
- Champion a culture of continuous improvement

## Codecool - Software Engineering Manager

**Nov 2021 - Dec 2022 (1 year)** 

- Built a product development team for a learning management platform (Java, Kotlin, Angular on GCP)
- Worked within a cross-functional team of PMs, UX designers, Engineers, Data Analyst, DevOps
- Introduced coding standards, engineering metrics, real product engineering in Dual track agile, data analytics
- Focused on technological decisions, architectural changes, code quality, simplicity

## Codecool - Head Of Education and Technology

**May 2018 - Nov 2021 (3 years)**

- Became a member of the global management team working closely with the CEO
- Responsible for the global education strategy, including platform/technology, content and methodology aspects
- Led the instructor/mentor teams of 5 campuses internationally (Hungary, Poland, Romania, Austria)
- Built an internal platform development team from scratch to support Codecool campuses

## Codecool - Lead Engineering Instructor

**Dec 2015 - May 2018 (2 years)**

- Built a new educational model and 1 year full-stack engineering course curriculum from scratch
- Led the start of the Budapest Codecool campus as Lead Instructor
- Mentored 100+ students within Codecool's 1 year programme

## Prezi.com - Engineering Manager

**Sept 2014 - Dec 2015 (1 year)**

- Led JUMP - an international "Junior Mentoring Program" at prezi.com
- Mentored 12 talented junior software engineers from various countries
- 100% success rate - all mentees found a full-time engineering job in the industry
- Later joined the Program Management team and led the alignment of 20+ engineering teams 

## Digital Natives - Founder, CTO

**Oct 2006 - Sep 2014 (8 years)**

- Built an outsourcing development company from scratch with 2 other founders
- Led the professional and personal development of the engineering team (primarily Ruby, Ruby on Rails)
- Managed the agile transformation, introduced SCRUM for product development

## Isobar (Kirowski) - Head of Development

**2004 - Oct 2006 (3 years)**

- Head of the development teams in the Technology Division
- Managed 20+ developers in different technologies (PHP, Java, .NET)
- Responsible for Strategy, Software Architecture, Resource and Project Planning, Internal Trainings 

## Isobar - Software Developer

**2002 - 2004 (3 years)**

- Worked as a full-stack engineer, mostly with Java, Javascript
- Built a Content Management System which were used in 50+ projects
- Participated in 100+ client (web app) projects within the agency


# Personal Interests

Mountaineering
: Walking, Trekking, Hiking, Climbing, Rock Climbing, everything with Mountains

Organisation Development
: Holacracy, Sociocracy, Decision making, Learning Organisations

Volunteering
: Actively involved in three public benefit non-profit organizations
